FROM perl:5

# install perl dependencies
RUN cpan Net::MQTT::Simple JSON \
  && mkdir -p /usr/src/app

WORKDIR /usr/src/app
COPY main.pl .
COPY lib/    lib/

ENTRYPOINT ["perl", "-Ilib", "main.pl"]
