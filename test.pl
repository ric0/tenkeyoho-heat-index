#! /urs/bin/perl
# Usage:
# $ perl -Ilib test.pl

use strict;
use warnings;
use JSON;

use HeatIndex qw(calculateHeatIndex);

print "T=15 RH=86 -> expecting " . HeatIndex::calculateHeatIndex(15, 86) . " to be 15\n";
print "T=25 RH=90 -> expecting " . HeatIndex::calculateHeatIndex(25, 90) . " to be 26\n";
print "T=30 RH=90 -> expecting " . HeatIndex::calculateHeatIndex(30, 90) . " to be 41\n";
print "T=30 RH=20 -> expecting " . HeatIndex::calculateHeatIndex(30, 20) . " to be 28\n";
print "T=35 RH=75 -> expecting " . HeatIndex::calculateHeatIndex(35, 75) . " to be 53\n";
print "T=35 RH=20 -> expecting " . HeatIndex::calculateHeatIndex(35, 20) . " to be 33\n";

exit;

# Trial and error playground:

my $variable;
print $variable;

if($variable) {
  print "true\n";
} else {
  print "false\n";
}

my $heatIndex = 3.14159;
my $message = to_json({"value" => $heatIndex, 42 => $heatIndex});
print $message . "\n";

system('touch alive2');
system('rm alive2');

die "test 3i3jkmk4";
