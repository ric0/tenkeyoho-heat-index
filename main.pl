#!/urs/bin/perl

use strict;
use warnings;

use JSON;
use Net::MQTT::Simple;
use HeatIndex qw(calculateHeatIndex);

my $host = "mqtt.cluster.tenkeyoho.cloud";
my $port = "1883";

print "Tenkeyoho - Heat Index\n";
print "----------------------\n";

print "Connecting to broker $host:$port...";
my $mqtt = Net::MQTT::Simple->new("$host:$port");
print " connected.\n";

# The temperature and humidity come from two different topics.
# Their values are stored in the following variables.
my $temperature;
my $humidity;

# k8s liveness probe: a simple file `alive`
# - remove the file on message
# - add the file after message has been processed
# this may be error prone 🤔
system('touch alive');

print "Listening on topics [/tenkeyoho/temperature] and [/tenkeyoho/humidity]...\n";
$mqtt->run(
    "/tenkeyoho/temperature" => sub {
        system('rm alive');
        my ($topic, $message) = @_;
        print "received [$topic] $message\n";

        $temperature = from_json($message)->{value};
        print " - updating temperature, new value: $temperature °C\n";
        updateHeatIndex();
        system('touch alive');
    },
    "/tenkeyoho/humidity" => sub {
        system('rm alive');
        my ($topic, $message) = @_;
        print "received [$topic] $message\n";

        $humidity = from_json($message)->{value};
        print " - updating humidity, new value $humidity\%RH\n";

        updateHeatIndex();
        system('touch alive');
    },
);

sub updateHeatIndex {
  if ($temperature && $humidity) {
    my $heatIndex = HeatIndex::calculateHeatIndex($temperature, $humidity);
    my $message = to_json({'name' => 'heat_index', 'value' => $heatIndex, 'unit' => '°C'});
    print " - publishing to [/tenkeyoho/heat_index] $message\n";
    $mqtt->publish("/tenkeyoho/heat_index" => $message);
  }
}
