package HeatIndex;

use strict;
use warnings;

# Calculate the Heat Index
#
# The heat index is an index that combines air temperature and relative humidity.
# The algorithm is from https://github.com/mcci-catena/heat-index/blob/master/heat-index.js
#
# @param  T  temperature in °C
# @param  RH humidity in % (0..100)
# @return HI Heat Index in °C
sub calculateHeatIndex {
  my ($T_Celsius, $RH) = @_;

  my $T_Fahrenheit = $T_Celsius * 1.8 + 32;
  my $T = convertToFahrenheit($T_Celsius);

  if($T < 76 || $T > 126 || $RH < 0 || $RH > 100) {
    return $T_Celsius;
  }

  my $HI_Simple = 0.5 * ($T + 61.0 + (($T - 68.0) * 1.2) + ($RH * 0.094));

  if (($HI_Simple + $T) < 160.0) {
    return convertToCelsius($HI_Simple);
  }

  my $HI =
    -42.379
    + 2.04901523  * $T
    + 10.14333127 * $RH
    - 0.22475541  * $T * $RH
    - 0.00683783  * $T*$T
    - 0.05481717  * $RH*$RH
    + 0.00122874  * $T*$T *$RH
    + 0.00085282  * $T * $RH*$RH
    - 0.00000199  * $T*$T * $RH*$RH;

  my $adjustment = 0;
  if($RH < 13.0 && 80.0 <= $T && $T <= 112.0) {
    $adjustment = -((13.0 - $RH) / 4.0) * sqrt((17.0 - abs($T - 95.0)) / 17.0);
  } elsif($RH > 85.0 && 80.0 <= $T && $T <= 87.0) {
     $adjustment = (($RH - 85.0) / 10.0) * ((87.0 - $T) / 5.0);
  }

  $HI = $HI + $adjustment;

  if($HI >= 183.5) {
    return $T_Celsius;
  }

  return convertToCelsius($HI);
}

sub convertToCelsius {
  my ($T_Fahrenheit) = @_;
  return ($T_Fahrenheit - 32) / 1.8;
}

sub convertToFahrenheit {
  my ($T_Celsius) = @_;
  return $T_Celsius * 1.8 + 32;
}

1;
