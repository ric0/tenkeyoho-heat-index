#!/urs/bin/perl

use strict;
use warnings;

use Net::MQTT::Simple;

my $host = "mqtt.cluster.tenkeyoho.cloud";
my $port = "1883";

my $mqtt = Net::MQTT::Simple->new("$host:$port");

$mqtt->run(
    "/tenkeyoho/#" => sub {
        my ($topic, $message) = @_;
        print "[$topic] $message\n";
    },
);
